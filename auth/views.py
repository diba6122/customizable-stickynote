from django.http import HttpResponse, Http404, HttpRequest, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.shortcuts import render
from django.contrib.auth import authenticate,login,logout
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.models import User as Member
from auth.forms import Login

def logout_user(request):
    if request.POST:
        logout(request)
        return HttpResponseRedirect(reverse('login')) 
    else: 
        return HttpResponseRedirect(reverse('notelist'))



def login_user(request):
    if request.method == "POST":
        form = Login(request.POST)
        if(request.user.is_authenticated()):
            if  request.user.is_superuser: 
                    usertype = 'superuser'
            else:
                    usertype = 'not superuser'
            request.session['user_id']=request.user.id
            return HttpResponseRedirect(reverse('notelist'))
        elif form.is_valid():
            user  =  authenticate(username=request.POST.get('username'), password= request.POST.get('password'))
            if user !=None:
                if user.is_active:
                    login(request,user)
                    if user.is_superuser:
                        usertype  = 'superuser'
                    else:
                        usertype = 'regular user'
                    request.session['user_id']=user.id
                    return HttpResponseRedirect(reverse('notelist'))
                return render_to_response('Authentication/auth.html',{'state':'you have to activate','username':request.POST.get('username'),'form':form},context_instance=RequestContext(request))
            return render_to_response('Authentication/auth.html',{'state':'you have entered a wrong username or password','username':request.POST.get('username'),'form':form},context_instance=RequestContext(request))
    else:
        form = Login(None)
        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse('notelist'))
    return render_to_response('Authentication/auth.html',{'state':'Please login here!','username':'','form':form},context_instance=RequestContext(request))
        