from django.conf.urls import patterns, include, url
from todo_app.views import shownotes
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from auth.views import login_user
from django.conf.global_settings import STATICFILES_DIRS
admin.autodiscover()


urlpatterns = patterns('',
                       url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': STATICFILES_DIRS}),
                       url(r'^$',shownotes,name='notelist'),
    # Examples:
    # url(r'^$', 'Notepad.views.home', name='home'),
    # url(r'^Notepad/', include('Notepad.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/$',login_user,name="login"),
    (r'^logout/$','auth.views.logout_user'),
    (r'^addnote/$','todo_app.views.addnote'),
    (r'^removenote/$','todo_app.views.removenote'),
    (r'^save_size/$', 'todo_app.views.save_size'),
)
