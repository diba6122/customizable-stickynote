from django.db import models
from django.contrib.auth.models import User as user
# Create your models here.

class Note(models.Model):
    body   =  models.CharField(max_length  = 20)
    complete = models.BooleanField(default=0)
    priority =  models.IntegerField()
    color =  models.CharField(max_length = 20) 
    userid =  models.ForeignKey(user)
    width = models.PositiveSmallIntegerField(default=250)
    height = models.PositiveSmallIntegerField(default=100)
    
    def __unicode__(self):
        return u'%s' % self.priority
    