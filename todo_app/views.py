# Create your views here.
from todo_app.models import Note
from django.http import Http404,HttpResponse,HttpResponseRedirect
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

def shownotes(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('login')) 
    else:
        user_id = request.session['user_id']
        notes =  Note.objects.filter(userid=user_id).order_by('priority')
    return render_to_response('index.html',{'notes':notes},context_instance = RequestContext(request))

def addnote(request):
    if request.POST:
        user_id = request.session['user_id']
        notes =  Note.objects.filter(userid=user_id).order_by('priority')
        for note in notes: 
            note.priority += 1
            note.save()
        new_note = Note()
        new_note.body =  'Add your note..'
        new_note.userid = request.user
        new_note.complete =  0
        new_note.priority = 1  
        new_note.color = 'yellow'
        new_note.save()
        return HttpResponseRedirect(reverse('notelist'))
    else: 
        raise Http404
   # return render_to_response('index.html',{},contex_instance= Request.)
   
@csrf_exempt
def removenote(request):
    if request.method=="POST": 
        user_id = request.session['user_id']
        note_id = request.POST.get("note_id", "")
        f = open('file1.txt','w')
        f.write(str(note_id))
        f.close()
        note  = Note.objects.filter(userid=user_id,id=note_id)
        note.delete()
        return HttpResponseRedirect(reverse('notelist'))
    else: 
        raise Http404
    
@csrf_exempt
def save_size(request):
    if request.is_ajax():
        if request.method == "POST":
            note = Note.objects.get(pk=request.POST['note_id'])
            note.width = request.POST['width']
            note.height = request.POST['height']
            
            note.save()
            
            return HttpResponseRedirect(reverse('notelist'))
            #data = serializers.serialize('json', note)
            #return HttpResponse(data, mimetype='application/json')
    else:
        raise Http404
        
        