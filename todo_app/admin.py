from django.contrib import admin
from todo_app.models import Note

class NoteAdmin(admin.ModelAdmin):
    
    search_fields = ('body','compleete')


admin.site.register(Note,NoteAdmin)